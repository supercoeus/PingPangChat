package com.pingpang.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;

import com.pingpang.websocketchat.ChartUser;

/**
   * 用户处理
 * @author dell
 */
@Mapper
public interface UserDao {

	/**
	  * 添加用户
	 * @param user
	 */
	public void addUser(ChartUser user);
	
	
	/**
	  * 修改用户
	 * @param user
	 */
	public void updateUser(ChartUser user);
	
    
	/**
	  *    获取用户
	 * @param user
	 * @return
	 */
	public ChartUser getUser(ChartUser user);
	
	
	/**
	  *   获取用户总数
	 * @param queryMap
	 * @return
	 */
	public int getAllUserCount(Map<String,String> queryMap);
	
	/**
	   * 获取所有用户数据
	 * @param searchMap
	 * @return
	 */
	public Set<ChartUser> getAllUser(Map<String,String> searchMap);
	
	
	/**
	   * 获取用户数据带密码的
	 * @return
	 */
	public Set<ChartUser> getRedisAllUser(Map<String,String> queryMap);
	
	/**
	  *    获取用户
	 * @param user
	 * @return
	 */
	public ChartUser getRedisUser(ChartUser user);
	
	/**
	   * 获取用户注册数据
	 * @param day
	 * @return
	 */
	public List<Map<String,String>> getUserRegsitCount(int day);
	
	/**
	  * 获取最近聊天得用户
	 * @param cu
	 * @return
	 */
	public Set<ChartUser> getUserOldChat(ChartUser user);
}
