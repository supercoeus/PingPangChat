package com.pingpang.websocketchat;

public class ChatUserBind {

	private String id;
	private String userCode;
	private String userIP;
	private String createDate;
	private String userType;//0注册成功，1登录成功，2绑定成功
	
	public ChatUserBind(String userCode,String userIP,String userType) {
		this.userCode=userCode;
		this.userIP=userIP;
		this.userType=userType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserIP() {
		return userIP;
	}
	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	@Override
	public String toString() {
		return "ChatUserBind [id=" + id + ", userCode=" + userCode + ", userIP=" + userIP + ", createDate=" + createDate
				+ ", userType=" + userType + "]";
	}
}
