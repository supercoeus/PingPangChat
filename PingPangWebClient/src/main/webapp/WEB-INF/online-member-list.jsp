<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
     <head>
        <meta charset="UTF-8">
        <title>PingPang管理页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
        <script src="${httpServletRequest.getContextPath()}/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" id="sreach">
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="userCode"  placeholder="请输入用代码" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="userName"  placeholder="请输入用户名" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="开始日" name="start" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="截止日" name="end" id="end">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type='button' class="layui-btn"  onclick="sreach();" value="查询">
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn layui-btn-xs" onclick="managerAll(1)"><i class="layui-icon"></i>批量禁言</button>
                            <button class="layui-btn layui-btn-warm" onclick="managerAll(2)"><i class="layui-icon"></i>批量上线</button>
                            <button class="layui-btn layui-btn-danger" onclick="managerAll(3)"><i class="layui-icon"></i>批量删除</button>
                        
                            <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input" style="width: 250px;" autocomplete="off" placeholder="广播" name="msg" id="msg">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                    <input type='button' class="layui-btn"  onclick="sendMsgAll();" value="推送">
                            </div>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="userList">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    <script>
    layui.use(['laydate','form','table'],
            function() {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#start' //指定元素
                });

                //执行一个laydate实例
                laydate.render({
                    elem: '#end' //指定元素
                });
                
                var table = layui.table;
                //第一个实例
                table.render({
                  elem: '#userList',
                  height: 312,
                  url: '${httpServletRequest.getContextPath()}/userController/online-list', //数据接口
                  method:'post',
                  contentType: 'application/json; charset=UTF-8',
                  initSort: {
                	    field: 'userCreateDate' //排序字段，对应 cols 设定的各字段名
                	    ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                	  },
                  //where:JSON.stringify(layui.form.field),
                  page: true, //开启分页
                  cols: [
                	      [
                	    	{type: 'checkbox',field: 'id',fixed:'left'},
    		                {field: 'userCode', title: '用户代码', width:90},
    		                {field: 'userName', title: '用户昵称', width:90},
    		                {field: 'userEmail', title: '邮箱', width:90},
    		                {field: 'userPhone', title: '用户电话', width:90},
    		                {field: 'userImagePath', title: '用户头像', width:90},
    		                {field: 'userSex', title: '性别', width:90,templet:'<div>{{ sexFormat(d.userSex)}}</div>'},
    		                {field: 'userCreateDate', title: '创建日期', width:90},
    		                {field: 'userStatus', title: '状态', width: 90,templet:'<div>{{ statusFormat(d.userStatus)}}</div>'},
    		                {field: 'right', title: '操作', width: 370, toolbar:"#barDemo"}
                          ]
                	    ]
                });
                
            });
    
    //性别翻译
    function sexFormat(statu) {
    	var msg="";
        if(0==statu){
        	msg="男";
        }else if(1==statu){
        	msg="女";
        }else{
        	msg=" 未识别 ";
        }
        return msg;
    }  
    
    //状态翻译
    function statusFormat(statu) {
    	var msg="";
        if(-1==statu){
        	msg="注销";
        }else if(0==statu){
        	msg="离线";
        }else if(1==statu){
        	msg="在线";
        }else if(2==statu){
        	msg="禁言";
        }else{
        	msg=" 未识别 ";
        }
        return msg;
    }  
    
    /**
     *type 1:线下 2:上线 3删除
     */
    function managerUser(id,type){
    	 var msg="";
    	 var url="";
    	 if(1==type){
    		 msg="确认要禁言吗？";
    		 url="${httpServletRequest.getContextPath()}/userController/online-downUser";
    	 }else if(2==type){
    		 msg="确认要上线吗？";
    		 url="${httpServletRequest.getContextPath()}/userController/online-upUser";
    	 }else if(3==type){
    		 msg="确认要删除吗？";
    		 url="${httpServletRequest.getContextPath()}/userController/online-delUser";
    	 }else{
    		 layer.msg('操作未识别!', {icon: 1});
   		     return false;
    	 }
    	 layer.confirm(msg,function(index){
       	  $.ajax({
       		  type: 'POST',
       		  async: false,
       		  dataType:'json',
       		  url:url,
       		  data:"id="+id,
       		  success: function(data){
       			  if("S"==data.CODE){
       				  layer.msg('执行成功', {icon: 1});
       				 // window.parent.location.reload();
       				  layui.table.reload("userList", { //此处是上文提到的 初始化标识id
       		                where: {
       		                    //key: { //该写法上文已经提到
       		                        //type: item.type, id: item.id
       		                    //}
       		                }
       		            });
       			  }else{
       				if(null==data.MESSAGE){
       				  layer.msg('执行失败', {icon: 1});
       				}else{
       				  layer.msg(data.MESSAGE, {icon: 1});
       				}
       			  }
       		  },
       		  error : function(errorMsg) {
                     layer.msg(errorMsg,{time:2000,end:function(){
       			  }});
       		  }
       		});
       });
    }
    
    
    function managerAll (type) {
    	var msg="";
   	    var url="";
    	
   	    if(1==type){
   		 msg="确认要禁言吗？";
   		 url="${httpServletRequest.getContextPath()}/userController/online-downUserAll";
   	    }else if(2==type){
   		 msg="确认要上线吗？";
   		 url="${httpServletRequest.getContextPath()}/userController/online-upUserAll";
   	    }else if(3==type){
   		 msg="确认要注销吗？";
   		 url="${httpServletRequest.getContextPath()}/userController/online-delUserAll";
   	    }else{
   	    	return;
   	    }
        var checkStatus = layui.table.checkStatus('userList').data;
         var ids = [];
         for(var i=0;i<checkStatus.length;i++){
       	  ids.push(checkStatus[i].userCode);
         }
         layer.confirm(msg,function(index){
             //捉到所有被选中的，发异步进行删除
              $.ajax({
               		  type: 'POST',
               		  async: false,
               		  dataType:'json',
               		  url:url,
               		  data:"ids="+ids.toString(),
               		  success: function(data){
               			  if("S"==data.CODE){
               				  layer.msg('执行成功', {icon: 1});
               				 // window.parent.location.reload();
               				  layui.table.reload("userList", { //此处是上文提到的 初始化标识id
               		                where: {
               		                    //key: { //该写法上文已经提到
               		                        //type: item.type, id: item.id
               		                    //}
               		                }
               		            });
               			  }
               			  //layer.msg(data.MESSAGE,{time:2000,end:function(){
               			  //}});
               		  },
               		  error : function(errorMsg) {
                             layer.msg(errorMsg,{time:2000,end:function(){
               			  }});
               		  }
               		});
         }); 
       }

    /**
     * 消息广播
     */
    function sendMsgAll () {
    	var msg=$("#msg").val();
   	    var url="${httpServletRequest.getContextPath()}/userController/online-sendUserAll";
    	
   	    if(null==msg || ""==msg || msg.trim().length<1){
   	    	layer.msg("请输入广播内容!", {icon: 1});
   	    	return;
   	    }
   	    
        var checkStatus = layui.table.checkStatus('userList').data;
         var ids = [];
         for(var i=0;i<checkStatus.length;i++){
       	  ids.push(checkStatus[i].userCode);
         }
         
         if(ids.length<1){
        	 layer.msg("请选择用户!", {icon: 1});
        	 return;
         }
         
	              $.ajax({
	               		  type: 'POST',
	               		  async: false,
	               		  dataType:'json',
	               		  url:url,
	               		  data:{"ids":ids.toString(),"msg":msg},
	               		  success: function(data){
	               			  if("S"==data.CODE){
	               				  layer.msg('执行成功', {icon: 1});
	               				  
	               				layui.table.reload("userList", { //此处是上文提到的 初始化标识id
               		                where: {
               		                    //key: { //该写法上文已经提到
               		                        //type: item.type, id: item.id
               		                    //}
               		                }
               		            });
	               			  }
	               		  },
	               		  error : function(errorMsg) {
	                             layer.msg(errorMsg,{time:2000,end:function(){
	               			  }});
	               		  }
	               		});
       }
    
    
    
    function sreach(){
    	var param=JSON.stringify(getFormJson($('#sreach')));
    	//alert(param);
    	layui.table.reload("userList", { //此处是上文提到的 初始化标识id
            where: {
            	search:param
            }
        });
    	return false;
    }
    

    //将form中的值转换为键值对。
    function getFormJson(frm) {
        var o = {dosubmit:1};
        var a = $(frm).serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
    </script>
    <script type="text/html" id="barDemo">
        <button type="button" class="layui-btn layui-btn-xs" onclick="managerUser('{{d.userCode}}','1')">禁言</button>
		<button type="button" class="layui-btn layui-btn-warm" onclick="managerUser('{{d.userCode}}','2')">上线</button>
        <button type="button" class="layui-btn layui-btn-danger" onclick="managerUser('{{d.userCode}}','3')">删除</button>
</script>
</html>