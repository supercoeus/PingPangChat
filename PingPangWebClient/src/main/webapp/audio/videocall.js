/**
 * 一对一视频服务
 */

var txtSelfId = document.querySelector("input#txtSelfId");
var txtTargetId = document.querySelector("input#txtTargetId");
//var localVideo = document.querySelector("video#localVideo");
//var remoteVideo = document.querySelector("video#remoteVideo");
var videoSelect = document.querySelector("select#videoSelect")

let peer = null;
let localConn = null;
let localStream = null;

hashCode = function (str) {
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
    return hash;
}

function gotStream(stream) {
    console.log('received local stream');
    localStream = stream;
    //localVideo.srcObject = localStream;
    localVideoVid.srcObject = stream;
    localVideoVid.play();
    
    setTimeout("audioCall()","1000");
}

function handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

//绑定摄像头列表到下拉框
function gotDevices(deviceInfos) {
    if (deviceInfos===undefined){
        return
    }
    for (let i = 0; i !== deviceInfos.length; ++i) {
        const deviceInfo = deviceInfos[i];
        const option = document.createElement('option');
        option.value = deviceInfo.deviceId;
        if (deviceInfo.kind === 'videoinput') {
            option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
            videoSelect.appendChild(option);
        }
    }
}

//开启本地摄像头
function start() {
    if (localStream) {
        localStream.getTracks().forEach(track => {
            track.stop();
        });
    }

    const videoSource = videoSelect.value;
    const constraints = {
        audio:  {
                 echoCancellation: true,
                 noiseSuppression: true,
                 autoGainControl: true,
                 sampleRate: 44100,
                 channelCount: 2,
                 volume:1.0
                 },
        video: { width: 320, deviceId: videoSource ? { exact: videoSource } : undefined }
    };

    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(gotStream)
        .then(gotDevices)
        .catch(handleError);
}


let connOption = { host: '139.159.144.33', port: 9000, path: '/', debug: 3 };

function audioCall() {
	
    if (!peer) {
        peer = new Peer(hashCode(txtSelfId.value), connOption);
        peer.on('open', function (id) {
            //console.log("register success. " + id);
        });
        peer.on('call', function (call) {
            call.answer(localStream);
        });
        
      //接受视频通话邀请
        var call = peer.call(hashCode(txtTargetId.value), localStream);
        
//        while($.isEmptyObject(call)){
//        	sleep(3000);
//        	call = peer.call(hashCode(txtTargetId.value), localStream);
//        }
        
        call.on('stream', function (stream) {
            console.log('received remote stream');
            //remoteVideo.srcObject = stream;
            //sleep(200);
            remoteVideoVid.srcObject = stream;
            remoteVideoVid.load();
            //remoteVideoVid.play();
            
            let playPromise = remoteVideoVid.play();
            if (playPromise !== undefined) {
                playPromise.then(() => {
                	remoteVideoVid.play();
                }).catch(()=> {
                   
                })
            }
            
        });
    }
}


window.onload = function () {
    if (!navigator.mediaDevices ||
        !navigator.mediaDevices.getUserMedia) {
        console.log('webrtc is not supported!');
        alert("webrtc is not supported!");
        return;
    }

    //获取摄像头列表
    navigator.mediaDevices.enumerateDevices()
        .then(gotDevices)
        .catch(handleError);

    videoSelect.onchange = start;

    start();
    
    //audioCall();
}
//setTimeout("audioCall()","1000");

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}