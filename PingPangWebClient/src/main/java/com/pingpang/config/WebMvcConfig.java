package com.pingpang.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.pingpang.interceptor.LoginHandlerInterceptor;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	/**
	 * 不需要登录拦截的url
	 */
	final String[] notLoginInterceptPaths = { "/audio/**",
			                                  "/css/**",
			                                  "/font/**",
			                                  "/images/**",
			                                  "/lay/**",
			                                  "/mobile/**",
			                                  "/theme/**",
			                                  "/x-admin/**",
			                                  "/static/**",
			                                  /* "/druid/**", */
			                                  "/jquery.min.js",
			                                  "/layer.js",
			                                  "/layui.all.js",
			                                  "/layui.js",
			                                  "/error",
			                                  "/user/chat",
			                                  "/user/index",
			                                  "/",//默认ip+端口 直接进入登录页面
			                                  "/userController/regist",
			                                  "/userController/addUser",
			                                  "/actuator/**",
			                                  "/autho/rtmp",//权限验证接口
			                                  "/autho/rtmpOver"//移除token
			                                  };

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns(notLoginInterceptPaths);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("classpath:/META-INF/resources/");
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// 设置系统访问的默认首页
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/").setViewName("blog");
	}
	
	/*
	 * @Override public void addViewControllers(ViewControllerRegistry registry) {
	 * registry.addViewController("/").setViewName("forward:/WEB-INF/index");
	 * registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	 * WebMvcConfigurer.super.addViewControllers(registry); }
	 */
}
